/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pegawai;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Rizal Ardhi R
 */
public class database {
    private static String url = "jdbc:mysql://localhost/db_pegawai?user=db_pegawai&password=1234";
    public static ResultSet get(String sql){
        try {
            Connection koneksi = DriverManager.getConnection(url);
            Statement statement = koneksi.createStatement();
            ResultSet hasil = null;
            if(statement.execute(sql)){
                hasil = statement.getResultSet();
                return hasil;
            }
        } catch (SQLException ex) {
            System.out.println("cek:"+ex.getMessage());
        }
        return null;
    }
    public static boolean execute(String sql){
        try {
            Connection koneksi = DriverManager.getConnection(url);
            Statement statement = koneksi.createStatement();
            
            statement.execute(sql);
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }
}
