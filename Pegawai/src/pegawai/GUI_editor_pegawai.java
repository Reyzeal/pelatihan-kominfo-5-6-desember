/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pegawai;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rizal Ardhi R
 */
public class GUI_editor_pegawai extends javax.swing.JFrame {
    String[][] DataJabatan;
    int pegawai_id;
    /**
     * Creates new form GUI_add_pegawai
     */
    public GUI_editor_pegawai() {
        initComponents();
        this.getJabatan();
        this.pegawai_id = -1;
    }
    public GUI_editor_pegawai(int pegawai_id){
        initComponents();
        this.getJabatan();
        this.pegawai_id = pegawai_id;
        try {
            ResultSet hasil = database.get("SELECT * FROM pegawai WHERE id = "+pegawai_id);
            if(hasil.next()){
                this.nip.setText(hasil.getString("nip"));
                this.nama.setText(hasil.getString("nama"));
                for(int i=0;i<this.DataJabatan.length;i++){;
                    if(this.DataJabatan[i][0].equals(hasil.getString("jabatan"))){
                        this.jabatan.setSelectedItem(this.DataJabatan[i][1]);
                    }
                }
                this.tanggal.setText(hasil.getString("tanggal_lahir"));
                String jk = hasil.getString("jenis_kelamin");
                if(jk.equals("Laki-laki"))
                    this.jenisKelamin.setSelected(this.lakiOpsi.getModel(), true);
                else
                    this.jenisKelamin.setSelected(this.perempuanOpsi.getModel(), true);
                this.alamat.setText(hasil.getString("alamat"));
                this.agama.setSelectedItem(hasil.getString("agama"));
            }
        } catch (SQLException ex) {
            
        }
    }
    public void getJabatan(){
        try {
            this.DataJabatan = null;
            ResultSet hasil = database.get("SELECT * FROM jabatan");
            
            int total = 0;
            if(hasil.last()){
                total = hasil.getRow();
                this.DataJabatan = new String[total][2];
                hasil.beforeFirst();
            }
            
            this.jabatan.removeAllItems();
            int baris = 0;
            while(hasil.next()){
                this.jabatan.addItem(hasil.getString(2));
                this.DataJabatan[baris][0] = hasil.getString(1);
                this.DataJabatan[baris][1] = hasil.getString(2);
                System.out.println(this.DataJabatan[baris][1]);
                baris++;
            }
        } catch (SQLException ex) {
            //Logger.getLogger(GUI_add_pegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jenisKelamin = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        nip = new javax.swing.JTextField();
        nama = new javax.swing.JTextField();
        tanggal = new javax.swing.JTextField();
        jabatan = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lakiOpsi = new javax.swing.JRadioButton();
        perempuanOpsi = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        simpanBtn = new javax.swing.JButton();
        batalBtn = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        alamat = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        agama = new javax.swing.JComboBox<>();

        jLabel3.setText("jLabel3");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Pegawai");

        jabatan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setText("NIP");

        jLabel4.setText("Nama");

        jLabel5.setText("Jabatan");

        jLabel6.setText("Tgl/Lahir");

        jenisKelamin.add(lakiOpsi);
        lakiOpsi.setSelected(true);
        lakiOpsi.setText("Laki-laki");

        jenisKelamin.add(perempuanOpsi);
        perempuanOpsi.setText("Perempuan");

        jLabel7.setText("Jenis Kelamin");

        simpanBtn.setText("Simpan");
        simpanBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanBtnActionPerformed(evt);
            }
        });

        batalBtn.setText("Batal");
        batalBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batalBtnActionPerformed(evt);
            }
        });

        jLabel9.setText("Alamat");

        alamat.setColumns(20);
        alamat.setRows(5);
        jScrollPane1.setViewportView(alamat);

        jLabel10.setText("Agama");

        agama.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Islam", "Kristen", "Katolik", "Budha", "Hindu" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(agama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(42, 42, 42)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lakiOpsi)
                                    .addComponent(perempuanOpsi)))
                            .addComponent(jLabel6)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel5))
                                    .addGap(47, 47, 47)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(simpanBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(batalBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(nama, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                                                    .addComponent(nip)))
                                            .addGap(63, 63, 63))
                                        .addComponent(tanggal)))
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tanggal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lakiOpsi)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(perempuanOpsi)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(agama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(simpanBtn)
                    .addComponent(batalBtn))
                .addContainerGap())
        );

        lakiOpsi.setActionCommand("Laki-laki");
        perempuanOpsi.setActionCommand("Perempuan");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void simpanBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanBtnActionPerformed
        String NIP      = this.nip.getText();
        String nama     = this.nama.getText();
        
        int index       = this.jabatan.getSelectedIndex();
        String jabatan  = this.DataJabatan[index][0];
        
        String tglLahir = this.tanggal.getText();
        
        String jenisKelamin = this.jenisKelamin.getSelection().getActionCommand();
        String alamat   = this.alamat.getText();
        String agama    = this.agama.getSelectedItem().toString();
        
        if(pegawai_id == -1){
            database.execute("INSERT INTO pegawai (nip,nama,jabatan,tanggal_lahir,jenis_kelamin,alamat,agama)"
                + "VALUES('"+NIP+"','"+nama+"','"+jabatan+"','"+tglLahir+"','"+jenisKelamin+"','"+alamat+"','"+agama+"')");
            ResultSet hasil = database.get("SELECT id FROM pegawai ORDER BY id DESC LIMIT 1");
            int pegawai_id = 0;
            try {
                if(hasil.next())
                    pegawai_id = Integer.parseInt(hasil.getString(1));
                GUI_keluarga windowKeluarga = new GUI_keluarga(pegawai_id);
                windowKeluarga.setVisible(true);
            } catch (SQLException ex) {
                //Logger.getLogger(GUI_add_pegawai.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            database.execute("UPDATE pegawai SET nip='"+NIP+"',nama='"+nama+"',jabatan='"+jabatan+"',tanggal_lahir='"+tglLahir+"',jenis_kelamin='"+jenisKelamin+"',alamat='"+alamat+"',agama='"+agama+"' WHERE id ="+this.pegawai_id);
            GUI_keluarga windowKeluarga = new GUI_keluarga(pegawai_id);
            windowKeluarga.setVisible(true);
            this.dispose();
        }
        
    }//GEN-LAST:event_simpanBtnActionPerformed

    private void batalBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batalBtnActionPerformed
        new GUI_utama().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_batalBtnActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI_editor_pegawai.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI_editor_pegawai.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI_editor_pegawai.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI_editor_pegawai.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUI_editor_pegawai(1).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> agama;
    private javax.swing.JTextArea alamat;
    private javax.swing.JButton batalBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jabatan;
    private javax.swing.ButtonGroup jenisKelamin;
    private javax.swing.JRadioButton lakiOpsi;
    private javax.swing.JTextField nama;
    private javax.swing.JTextField nip;
    private javax.swing.JRadioButton perempuanOpsi;
    private javax.swing.JButton simpanBtn;
    private javax.swing.JTextField tanggal;
    // End of variables declaration//GEN-END:variables
}
